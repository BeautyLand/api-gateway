
import fetch  from 'node-fetch'
import Lokka from 'lokka'
import Transport from 'lokka-transport-http'

global.fetch = fetch

const handleErrors = (errors, data) => {
  const message = errors[0].message;
  const error = new Error(`GraphQL Error: ${message}`);
  error.rawErrors = errors;
  error.rawData = data;
  throw error;
}

const url = `http://127.0.0.1:${process.env.PORT}${process.env.GRAPHQL_PATH}`

const createClient = (token) => {
  const options = {
    handleErrors
  }
  if (token) {
    options.headers = {
      authorization: `Bearer ${token}`
    }
  }
  const client = new Lokka({
    transport: new Transport(url, options)
  })
  return client
}

exports.query = (query, variables, token) => {
  const client = createClient(token)
  return client.query(query, variables, token)
}

exports.mutate = (mutation, variables, token) => {
  const client = createClient(token)
  return client.mutate(mutation, variables)
}
