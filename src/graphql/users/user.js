import {
  GraphQLID,
  GraphQLInterface,
  GraphQLNonNull,
  GraphQLString
} from 'graphql'
import userType from './userType'


const user = {
  type: userType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
      description: ''
    }
  },
  async resolve(_, {id}) {
    return {}
  }
}

export default user
