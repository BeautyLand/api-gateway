import { GraphQLObjectType } from 'graphql'

import viewerType from './viewerType'

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: {
    viewer: {
      type: viewerType,
      resolve: () => ({})
    }
  }
})

export default queryType
