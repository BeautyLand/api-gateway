import { GraphQLSchema } from 'graphql'
import mutationType from'./mutationType'
import queryType from './queryType'

const schema = new GraphQLSchema({
  query: queryType,
  //mutation: mutationType
})

export default schema
