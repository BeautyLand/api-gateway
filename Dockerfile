FROM node:argon

RUN mkdir /api-gateway

WORKDIR /api-gateway

COPY package.json /api-gateway

COPY . /api-gateway

RUN npm install babel-cli@6.10.1 -g --save

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
