import { GraphQLObjectType } from 'graphql'

const mutationType = new GraphQLObjectType({
   name: 'Mutation',
    fields: {
      user:{}
    }
})

export default mutationType
