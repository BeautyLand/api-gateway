import { GraphQLObjectType } from 'graphql'
import user from './users/user'

const viewerType = new GraphQLObjectType({
  name: 'Viewer',
  fields: {
    user
  }
})

export default viewerType
