import winston from 'winston'
import WinstonTcpGraylog from 'winston-tcp-graylog'

let env  = process.env.NODE_ENV;

var options = {
  gelfPro: {
    adapterName: 'tcp',
    adapterOptions: {
      host: 'graylog',
      port: 12201
    }
  }
}

let wConsole = new winston.transports.Console()
let wGraylog = new winston.transports.TcpGraylog(options)

let transports =[ wConsole ]

if (env !== 'development') {
  transports.push(wGraylog)
}

var logger = new winston.Logger({
  transports: transports
})

logger
  .on('error', err => {
    // internal winston problems
    console.error('!error: ', err)
  })

wGraylog
  .on('error', err => {
    // internal WinstonTcpGraylog problems
    console.error('!wtg:error: ', err)
  })
  .on('send', (msg, res) => {
    // only WinstonTcpGraylog "logging"
    console.info('!wtg:send: ', msg, res)
  })

export default logger
