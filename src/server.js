import express from 'express';
import logger from '../lib/logger'
import cors from 'cors'
import graphqlHTTP from 'express-graphql'

import schema from './graphql/schema'

let server

exports.start = async () => {
  try {
    const app = express()
    app.use(cors())
    app.use(process.env.GRAPHQL_PATH, graphqlHTTP(async (request) => {
      return {
        context: {
          logger: logger,
        },
        formatError: error => {
          logger.info('Failed to process GraphQL request.', error)
          return {
            locations: error.locations,
            message: error.message,
            path: error.path,
            validationErrors: error.originalError && error.originalError.validationErrors
          }
        },
        extensions: ({ document, variables, operationName, result }) => {
          logger.info('Processed GraphQL request.', {durationInMilliseconds: new Date() - Date.now()})
        },
        graphiql: true,
        schema
      }
    }))
    server = await app.listen(process.env.PORT)
    logger.info('Started GraphQL server.', {port: process.env.PORT})
  }
  catch (error) {
    logger.error('Failed to start GraphQL server.', error)
  }
}

exports.stop = async () => {
  try {
    await server.close()
    logger.info('Stopped GraphQL server.')
  }
  catch (error) {
    logger.error('Failed to stop GraphQL server.', error)
  }
}
