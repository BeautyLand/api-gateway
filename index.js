import server from "./src/server"
import env from 'dotenv'

env.config()

server.start()
